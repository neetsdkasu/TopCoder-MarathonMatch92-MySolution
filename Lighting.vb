Option Explicit On
Imports System
Imports System.Collections.Generic
Imports P = System.Tuple(Of Integer, Integer)
Imports Q = System.Tuple(Of System.Tuple(Of Integer, Integer, Integer), Double)

Public Class Lighting
    Public Function setLights(map() As String, D As Integer, L As Integer) As String()
        Dim S As Integer = map.Length - 1
        Dim Z As Integer = 10
        Dim Z1 As Integer = Z - 1
        Dim SZ As Integer = map.Length * Z - 1
        Dim DZ As Integer = D * Z
        Dim memo(3, SZ, SZ) As Integer
        Dim memoZ(3, SZ, SZ) As Integer
        Dim table(SZ, SZ) As Integer
        Dim answer As New List(Of String)(L)
        Dim power As Integer = 100 \ Z
        Dim offset As Integer = power \ 2
        For i As Integer = 0 To SZ
            For j As Integer = 0 To SZ
                table(i, j) = Integer.MaxValue
            Next j
        Next i
        For w As Integer = 1 To L
            For i As Integer = 0 To S
                If map(0).Chars(i) = "." Then
                    For j As Integer = i * Z To (i + 1) * Z - 1
                        Dim v As Integer = 1 And table(0, j)
                        memo(0, 0, j) = v
                        memoZ(0, 0, j) = v
                        memoZ(1, 0, j) = v
                    Next j
                    For y As Integer = 1 To Z1
                        For x As Integer = i * Z To (i + 1) * Z - 1
                            memo(0, y, x) = (memo(0, y - 1, x) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(i).Chars(S) = "." Then
                    For j As Integer = i * Z To (i + 1) * Z - 1
                        Dim v As Integer = 1 And table(j, SZ)
                        memo(1, j, SZ) = v
                        memoZ(1, j, SZ) = v
                        memoZ(2, j, SZ) = v
                    Next j
                    For y As Integer = i * Z To (i + 1) * Z - 1
                        For x As Integer = SZ - 1 To 0 Step -1
                            memo(1, y, x) = (memo(1, y, x + 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(S).Chars(i) = "." Then
                    For j As Integer = i * Z To (i + 1) * Z - 1
                        Dim v As Integer = 1 And table(SZ, j)
                        memo(2, SZ, j) = v
                        memoZ(2, SZ, j) = v
                        memoZ(3, SZ, j) = v
                    Next j
                    For y As Integer = SZ - 1 To SZ - Z1 Step - 1
                        For x As Integer = i * Z To (i + 1) * Z - 1
                            memo(2, y, x) = (memo(2, y + 1, x) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(i).Chars(0) = "." Then
                    For j As Integer = i * Z To (i + 1) * Z - 1
                        Dim v As Integer = 1 And table(j, 0)
                        memo(3, i, 0) = v
                        memoZ(3, j, 0) = v
                        memoZ(0, j, 0) = v
                    Next j
                    For y As Integer = i * Z To (i + 1) * Z - 1
                        For x As Integer = 1 To Z1
                            memo(3, y, x) = (memo(3, y, x - 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
            Next i
            For i As Integer = 1 To S
                Dim k As Integer = S - i
                For j As Integer = 0 To S
                    If map(i).Chars(j) = "." Then
                        For y As Integer = i * Z To (i + 1) * Z - 1
                            For x As Integer = j * Z To (j + 1) * Z - 1
                                memo(0, y, x) = (memo(0, y - 1, x) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                    If map(j).Chars(k) = "." Then
                        For y As Integer = j * Z To (j + 1) * Z - 1
                            For x As Integer = (k + 1) * Z - 1 To k * Z Step -1
                                memo(1, y, x) = (memo(1, y, x + 1) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                    If map(k).Chars(j) = "." Then
                        For y As Integer = (k + 1) * Z - 1 To k * Z Step -1
                            For x As Integer = j * Z To (j + 1) * Z - 1
                                memo(2, y, x) = (memo(2, y + 1, x) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                    If map(j).Chars(i) = "." Then
                        For y As Integer = j * Z To (j + 1) * Z - 1
                            For x As Integer = i * Z To (i + 1) * Z - 1
                                memo(3, y, x) = (memo(3, y, x - 1) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                Next j
            Next i
            If map(0).Chars(0) = "." Then
                For i As Integer = 1 To Z1
                    For j As Integer = 1 To Z1
                        memoZ(0, i, j) = (memoZ(0, i - 1, j - 1) + 1) And table(i, j)
                    Next j
                Next i
            End If
            If map(0).Chars(S) = "." Then
                For i As Integer = 1 To Z1
                    For j As Integer = SZ - 1 To SZ - Z1 Step - 1
                        memoZ(1, i, j) = (memoZ(1, i - 1, j + 1) + 1) And table(i, j)
                    Next j
                Next i
            End If
            If map(S).Chars(S) = "." Then
                For i As Integer = SZ - 1 To SZ - Z1 Step -1
                    For j As Integer = SZ - 1 To SZ - Z1 Step - 1
                        memoZ(2, i, j) = (memoZ(2, i + 1, j + 1) + 1) And table(i, j)
                    Next j
                Next i
            End If
            If map(S).Chars(0) = "." Then
                For i As Integer = SZ - 1 To SZ - Z1 Step -1
                    For j As Integer = 1 To Z1
                        memoZ(3, i, j) = (memoZ(3, i + 1, j - 1) + 1) And table(i, j)
                    Next j
                Next i
            End If
            For i As Integer = 1 To S
                Dim k As Integer = S - i
                If map(0).Chars(i) = "." Then
                    For y As Integer = 1 To Z1
                        For x As Integer = i * Z To (i + 1) * Z - 1
                            memoZ(0, y, x) = (memoZ(0, y - 1, x - 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(i).Chars(0) = "." Then
                    For y As Integer = i * Z To (i + 1) * Z - 1
                        For x As Integer = 1 To Z1
                            memoZ(0, y, x) = (memoZ(0, y - 1, x - 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(0).Chars(k) = "." Then
                    For y As Integer = 1 To Z1
                        For x As Integer = (k + 1) * Z - 1 To k * Z Step -1
                            memoZ(1, y, x) = (memoZ(1, y - 1, x + 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(S).Chars(i) = "." Then
                    For y As Integer = i * Z To (i + 1) * Z - 1
                        For x As Integer = SZ - 1 To SZ - Z1 Step -1
                            memoZ(1, y, x) = (memoZ(1, y - 1, x + 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(k).Chars(S) = "." Then
                    For y As Integer = (k + 1) * Z - 1 To k * Z Step -1
                        For x As Integer = SZ - 1 To SZ - Z1 Step - 1
                            memoZ(2, y, x) = (memoZ(2, y + 1, x + 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(S).Chars(k) = "." Then
                    For y As Integer = SZ - 1 To SZ - Z1 Step -1
                        For x As Integer = (k + 1) * Z - 1 To k * Z Step -1
                            memoZ(2, y, x) = (memoZ(2, y + 1, x + 1) + 1) And table(y, x)
                        Next x
                    Next y
                End If
                If map(S).Chars(i) = "." Then
                    For y As Integer = SZ - 1 To SZ - Z1 Step -1
                        For x As Integer = i * Z To (i + 1) * Z - 1
                            memoZ(3, y, x) = (memoZ(3, y + 1, x - 1) + 1) And table(y, x)
                        Next x
                    Next y                
                End If
                If map(k).Chars(0) = "." Then
                    For y As Integer = (k + 1) * Z - 1 To k * Z Step -1
                        For x As Integer = 1 To Z1
                            memoZ(3, y, x) = (memoZ(3, y + 1, x - 1) + 1) And table(y, x)
                        Next x
                    Next y                
                End If
            Next i
            For i As Integer = 1 To S
                Dim k As Integer = S - i
                For j As Integer = 1 To S
                    Dim m As Integer = S - j
                    If map(i).Chars(j) = "." Then
                        For y As Integer = i * Z To (i + 1) * Z - 1
                            For x As Integer = j * Z To (j + 1) * Z - 1
                                memoZ(0, y, x) = (memoZ(0, y - 1, x - 1) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                    If map(i).Chars(m) = "." Then
                        For y As Integer = i * Z To (i + 1) * Z - 1
                            For x As Integer = (m + 1) * Z - 1 To m * Z Step -1
                                memoZ(1, y, x) = (memoZ(1, y - 1, x + 1) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                    If map(k).Chars(m) = "." Then
                        For y As Integer = (k + 1) * Z - 1 To k * Z Step -1
                            For x As Integer = (m + 1) * Z - 1 To m * Z Step -1
                                memoZ(2, y, x) = (memoZ(2, y + 1, x + 1) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                    If map(k).Chars(j) = "." Then
                        For y As Integer = (k + 1) * Z - 1 To k * Z Step -1
                            For x As Integer = j * Z To (j + 1) * Z - 1
                                memoZ(3, y, x) = (memoZ(3, y + 1, x - 1) + 1) And table(y, x)
                            Next x
                        Next y
                    End If
                Next j
            Next i
            Dim maxcount As Integer = 0
            Dim maxposy As Integer = 0
            Dim maxposx As Integer = 0
            For i As Integer = 0 To S
                For j As Integer = 0 To S
                    If map(i).Chars(j) = "#" Then Continue For
                    For y As Integer = i * Z To (i + 1) * Z - 1
                        For x As Integer = j * Z To (j + 1) * Z - 1
                            Dim count As Integer = 0
                            For k As Integer = 0 To 3
                                count += Math.Min(DZ, memo(k, y, x))
                                count += Math.Min(DZ, memoZ(k, y, x))
                            Next k
                            If y - Z >= 0 Then
                                count += Math.Min(DZ - Z, memoZ(0, y - Z, x))
                                count += Math.Min(DZ - Z, memoZ(1, y - Z, x))
                                If map((y - Z) \ Z).Chars(x \ Z) = "." Then
                                    If x + Z <= SZ Then count += Math.Min(DZ - Z, memo(0, (y - Z) \ Z, (x + Z) \ Z))
                                    If x - Z >= 0 Then count += Math.Min(DZ - Z, memo(0, (y - Z) \ Z, (x - Z) \ Z))
                                End If
                            End If
                            If x + Z <= SZ Then
                                count += Math.Min(DZ - Z, memoZ(1, y, x + Z))
                                count += Math.Min(DZ - Z, memoZ(2, y, x + Z))
                                If map(y \ Z).Chars((x + Z) \ Z) = "." Then
                                    If y + Z <= SZ Then count += Math.Min(DZ - Z, memo(1, (y + Z) \ Z, (x + Z) \ Z))
                                    If y - Z >= 0 Then count += Math.Min(DZ - Z, memo(1, (y - Z) \ Z, (x + Z) \ Z))
                                End If
                            End If
                            If y + Z <= SZ Then
                                count += Math.Min(DZ - Z, memoZ(2, y + Z, x))
                                count += Math.Min(DZ - Z, memoZ(3, y + Z, x))
                                If map((y + Z) \ Z).Chars(x \ Z) = "." Then
                                    If x + Z <= SZ Then count += Math.Min(DZ - Z, memo(2, (y + Z) \ Z, (x + Z) \ Z))
                                    If x - Z >= 0 Then count += Math.Min(DZ - Z, memo(2, (y + Z) \ Z, (x - Z) \ Z))
                                End If
                            End If
                            If x - Z >= 0 Then
                                count += Math.Min(DZ - Z, memoZ(3, y, x - Z))
                                count += Math.Min(DZ - Z, memoZ(0, y, x - Z))
                                If map(y \ Z).Chars((x - Z) \ Z) = "." Then
                                    If y + Z <= SZ Then count += Math.Min(DZ - Z, memo(3, (y + Z) \ Z, (x - Z) \ Z))
                                    If y - Z >= 0 Then count += Math.Min(DZ - Z, memo(3, (y - Z) \ Z, (x - Z) \ Z))
                                End If
                            End If
                            If count > maxcount Then
                                maxcount = count
                                maxposy = y
                                maxposx = x
                            End IF
                        Next x
                    Next y
                Next j
            Next i
            Dim y0 As Integer = Math.Max(maxposy - DZ, 0)
            Dim y1 As Integer = Math.Min(maxposy + DZ, SZ)
            Dim x0 As Integer = Math.Max(maxposx - DZ, 0)
            Dim x1 As Integer = Math.Min(maxposx + DZ, SZ)
            For y As Integer = y0 To y1
                Dim dy As Integer = maxposy - y
                For x As Integer = x0 To x1
                    If y = maxposy And x = maxposx Then
                        table(y, x) = 0
                        Continue For
                    End If
                    If table(y, x) = 0 Then Continue For
                    Dim dx As Integer = maxposx - x
                    Dim dist As Double = Math.Sqrt(CDbl(dx * dx + dy * dy))
                    If dist > CDbl(DZ) Then Continue For
                    Dim nowall As Boolean = True
                    Dim dt As Integer = Math.Max(Math.Abs(dx), Math.Abs(dy))
                    For j As Integer = 0 To dt
                        Dim ty As Integer = y + j * dy \ dt
                        Dim tx As Integer = x + j * dx \ dt
                        If map(ty \ Z).Chars(tx \ Z) = "#" Then
                            nowall = False
                            Exit For
                        End If
                    Next j
                    If nowall Then table(y, x) = 0
                Next x
            Next y
            Dim yl As Integer = maxposy \ Z
            Dim yr As Integer = (maxposy Mod Z) * power + offset
            Dim xl As Integer = maxposx \ Z
            Dim xr As Integer = (maxposx Mod Z) * power + offset
            answer.Add(String.Format("{0}.{1:D2} {2}.{3:D2}", xl, xr, yl, yr))
        Next w
        setLights = answer.ToArray()
    End Function
End Class