Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Dim S, D, L As Integer
        Dim map(), answer() As String
        S = CInt(Console.ReadLine())
        ReDim map(S - 1)
        For i As Integer = 0 To S - 1
            map(i) = Console.ReadLine()
        Next i
        D = CInt(Console.ReadLine())
        L = CInt(Console.ReadLine())
        Dim lighting As New Lighting()
        answer = lighting.setLights(map, D, L)
        Console.WriteLine(answer.Length)
        For Each point As String In answer
            Console.WriteLine(point)
        Next point
    End Sub

End Module